<?php

/**
 * This stop-gap measure returns an existing participant's login credentials given their email address.
 * Password get reset and new pass returned.
 *
 * @param $email
 */
function _user_resource_email_login($email) {

  if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email)) {
    return services_error(t('invalid email address'), 400);
  }

  $user = user_load_by_mail($email);

  if (!$user) {
    return services_error(t('user not found'), 404);
  }

  if ($user->uid == 1) {
    return services_error(t('unable to reset password for user 1'), 403);
  }

  if (user_access('email login exclude', $user)) {
    return services_error(t("email's account excluded by role"), 403);
  }

  $changes = array(
    'pass' => user_password()
  );

  user_save($user, $changes);

  return array(
    'name' => $user->name,
    'pass' => $changes['pass']
  );
}

